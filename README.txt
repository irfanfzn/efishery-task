PHP + Laravel
* Diasumsikan composer, apache, php, mysql sudah ter-install

- Clone repo ini git@bitbucket.org:irfanfzn/efishery-task.git.
- run composer update.
- copy paste .env.example lalu rename salah satunya menjadi .env
- sesuaikan configurasi database dengan mengubahnya di file .env
- run php artisan migarte.
- run php artisan serve melalui terminal.

CASE 1 & 2
- lalu hit localhost:8000/api/authenticate dengan method post dan inputkan parameter yang dibutuhkan melalui postman.
- Pada bagian ini secara otomatis user akan membuatkan user baru jika belum ada lalu menampilkan response jwt token.

CASE 3
- hit localhost:8000/api/authenticate?token=... isi parameter token dengan jwt yang dihasilkan dari case diatas.
- response yang ditampilkan adalah private claims dari jwt token tersebut.

PYTHON + FLASK
* Diasumsikan python, flask, flask-jwt sudah terinstall

- run python jwtauth.py
- hit localhost:5000/api/private lalu isi header Authorization dengan JWT [jwt token yang didapat dari aplikasi laravel]
- response yang ditampilkan adalah private claims dari jwt token tersebut
